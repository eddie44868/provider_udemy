import 'package:equatable/equatable.dart';
import 'package:flutter_state_notifier/flutter_state_notifier.dart';
part 'temp_settings_state.dart';

class TempsSettingsProvider extends StateNotifier<TempsSettingsState> {
  TempsSettingsProvider() : super(TempsSettingsState.initial());

  void toggleTempUnit() {
    state = state.copyWith(
      tempUnit: state.tempUnit == TempUnit.celcius
            ? TempUnit.fahrenheit
            : TempUnit.celcius
    );
  }
}