part of 'temp_settings_provider.dart';

enum TempUnit {
  celcius,
  fahrenheit
}

class TempsSettingsState extends Equatable {
  final TempUnit tempUnit;
  TempsSettingsState({
    this.tempUnit = TempUnit.celcius,
  });

  factory TempsSettingsState.initial() {
    return TempsSettingsState();
  }

  @override
  List<Object> get props => [tempUnit];

  @override
  String toString() => 'TempsSettingsState(tempUnit: $tempUnit)';

  TempsSettingsState copyWith({
    TempUnit? tempUnit,
  }) {
    return TempsSettingsState(
      tempUnit: tempUnit ?? this.tempUnit,
    );
  }
}
