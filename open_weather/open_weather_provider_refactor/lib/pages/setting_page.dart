import 'package:flutter/material.dart';
import 'package:open_weather_provider/providers/providers.dart';
import 'package:provider/provider.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Setting'),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListTile(
          title: Text('Temperature Unit'),
          subtitle: Text('Celcius/Fahrenheit (Default: Celcius)'),
          trailing: Switch(
              value: context.watch<TempsSettingsProvider>().state.tempUnit ==
                  TempUnit.celcius,
              onChanged: (_) {
                context.read<TempsSettingsProvider>().toggleTempUnit();
              }
          ),
        ),
      ),
    );
  }
}