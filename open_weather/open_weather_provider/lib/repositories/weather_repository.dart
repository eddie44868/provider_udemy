// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:open_weather_provider/exceptions/weather_exception.dart';
import 'package:open_weather_provider/model/custom_error.dart';
import 'package:open_weather_provider/model/direct_geocoding.dart';
import 'package:open_weather_provider/model/weather.dart';
import 'package:open_weather_provider/services/weather_api_services.dart';

class WeatherRepository {
  final WeatherApiServices weatherApiServices;
  const WeatherRepository({
    required this.weatherApiServices,
  });

  Future<Weather> fetchWeather(String city) async {
    try {
      final DirectGeocoding directGeocoding =
          await weatherApiServices.getDirectGeocoding(city);
      final Weather tempWeather =
          await weatherApiServices.getWeather(directGeocoding);

      final Weather weather = tempWeather.copyWith(
          name: directGeocoding.name, country: directGeocoding.country);

      return weather;
    } on WeatherException catch(e) {
      throw CustomError(errMsg: e.message);
    }
     catch (e) {
      throw CustomError(errMsg: e.toString());
    }
  }
}
