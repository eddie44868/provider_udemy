class WeatherException implements Exception {
  String message;
  WeatherException([this.message = 'Something waent wrong']) {
    message = 'Weather Exception: $message';
  }

  @override
  String toString() {
    return message;
  }
}