import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
part 'temp_settings_state.dart';

class TempsSettingsProvider with ChangeNotifier {
  TempsSettingsState _state = TempsSettingsState.initial();
  TempsSettingsState get state => _state;

  void toggleTempUnit() {
    _state = _state.copyWith(
      tempUnit: _state.tempUnit == TempUnit.celcius
            ? TempUnit.fahrenheit
            : TempUnit.celcius
    );
    notifyListeners();
  }
}