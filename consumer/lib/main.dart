// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'models/dog.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<Dog>(
      create: (context) => Dog(name: 'dog05', breed: 'breed05', age: 3),
      child: MaterialApp(
        title: 'Provider',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: const MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Provider'),
        ),
        body: Consumer<Dog>(
          builder: (context, dog, child) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  child!,
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    // '- name : ${Provider.of<Dog>(context, listen: false).name}',
                    //'- name : ${context.watch<Dog>().name}',
                    '- name : ${dog.name}',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  BreedAndAge(),
                ],
              ),
            );
          },
          child: Text(
            'I like dogs very much',
            style: TextStyle(
              fontSize: 20,
            ),
          ),
        ));
  }
}

class BreedAndAge extends StatelessWidget {
  BreedAndAge({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<Dog>(builder: (_, dog, __) {
      return Column(
        children: [
          Text(
            // '- breed : ${Provider.of<Dog>(context, listen: false).breed}',
            // '- breed : ${context.select<Dog, String>((Dog dog) => dog.breed)}',
            '- breed : ${dog.breed}',
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Age(),
        ],
      );
    });
  }
}

class Age extends StatelessWidget {
  const Age({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<Dog>(builder: (_, dog, __) {
      return Column(
        children: [
          Text(
            // '- Age : ${Provider.of<Dog>(context).age}',
            // '- Age : ${context.select<Dog, int>((Dog dog) => dog.age)}',
            '- Age : ${dog.age}',
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
              onPressed: () =>
                  // Provider.of<Dog>(context, listen: false).grow(),
                  // context.read<Dog>().grow(),
                  dog.grow(),
              child: Text(
                'Grow',
                style: TextStyle(fontSize: 20),
              ))
        ],
      );
    });
  }
}
