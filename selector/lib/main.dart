// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'models/dog.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<Dog>(
      create: (context) => Dog(name: 'dog10', breed: 'breed10', age: 3),
      child: MaterialApp(
        title: 'Provider 10',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: const MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Provider 10'),
        ),
        body: Selector<Dog, String>(
          selector: (context, dog) => dog.name,
          builder: (context, name, child) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  child!,
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    '- name : $name',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  BreedAndAge(),
                ],
              ),
            );
          },
          child: Text(
            'I like dogs very much',
            style: TextStyle(
              fontSize: 20,
            ),
          ),
        ));
  }
}

class BreedAndAge extends StatelessWidget {
  BreedAndAge({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Selector<Dog, String>(
      selector: (context, dog) => dog.breed,
      builder: (_, breed, __) {
      return Column(
        children: [
          Text(
            '- breed : $breed',
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Age(),
        ],
      );
    });
  }
}

class Age extends StatelessWidget {
  const Age({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Selector<Dog, int>(
      selector: (context, dog) => dog.age,
      builder: (_, age, __) {
      return Column(
        children: [
          Text(
            '- Age : $age',
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
              onPressed: () =>
                  context.read<Dog>().grow(),
              child: Text(
                'Grow',
                style: TextStyle(fontSize: 20),
              ))
        ],
      );
    });
  }
}
