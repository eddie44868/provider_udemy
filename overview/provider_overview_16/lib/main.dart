import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_overview_16/pages/counter_page.dart';
import 'package:provider_overview_16/provider/counter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => Counter(),
      child: MaterialApp(
        title: 'AddPostFrameCallback',
        debugShowCheckedModeBanner: true,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: ListView(
          shrinkWrap: true,
          children: [
            ElevatedButton(
                onPressed: () => Navigator.push(
                    context, MaterialPageRoute(builder: (_) => CounterPage())),
                child: Text(
                  'Counter Page',
                  style: TextStyle(fontSize: 20),
                )),
          ],
        ),
      )),
    );
  }
}
